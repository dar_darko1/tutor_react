import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import {render} from '@testing-library/react'
import Title from '../components/Title'

test('renders content',()=>{
    const component = render(<Title/>)
    component.getByText('Command Line')
})